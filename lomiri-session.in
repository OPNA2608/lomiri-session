#!/bin/bash
# -*- Mode: sh; indent-tabs-mode: nil; tab-width: 2 -*-

# Ensure the shell always gets unthrottled touch events, so that applications
# who want full speed low-latency input can get it (LP: #1497105) and so that
# apps can use QML touch compression safely (the QML touch compression
# algorithm does not support nesting well - LP: #1486341, LP: #1556763 - so
# must be fed by the raw input event stream from Unity8).
export QML_NO_TOUCH_COMPRESSION=1


if [ "$XDG_SESSION_DESKTOP" = "ubuntu-touch" ]; then
  export MIR_SERVER_PROMPT_FILE=1

  # Hard code socket path because our snappy apparmor profile
  # only lets us put the socket in one place.  And consumers expect it there.
  # (XDG_RUNTIME_DIR isn't typical under snappy)
  export MIR_SERVER_FILE=/run/user/$(id -u)/mir_socket

  rm -f "$MIR_SERVER_FILE"
  rm -f "${MIR_SERVER_FILE}_trusted"

  # On Ubuntu Touch, we currently use a trick where we auto-login the
  # user without a LightDM greeter at all.  In this case, we want to
  # start with the lockscreen visible.  Once we switch to using a
  # proper greeter for the first login, we can remove this code path.
  MODE=full-greeter
else
  MODE=full-shell
fi

if [ -x "$(command -v device-info)" ]; then
    export GRID_UNIT_PX=$(device-info get GridUnit)
    export QTWEBKIT_DPR=$(device-info get WebkitDpr)
    export NATIVE_ORIENTATION=$(device-info get PrimaryOrientation)
    export QT_WAYLAND_FORCE_DPI=$((12 * ${GRID_UNIT_PX}))

    dbus-update-activation-environment --systemd QT_WAYLAND_FORCE_DPI=${QT_WAYLAND_FORCE_DPI}
    dbus-update-activation-environment --systemd GRID_UNIT_PX=${GRID_UNIT_PX}
fi

# Hack for virtual machines that fails to probe modeset
if ([ -x "$(command -v hostnamectl)" ] && [ "$(hostnamectl status | grep 'Chassis: vm')" ]) \
   || grep -wq hypervisor /proc/cpuinfo; then
    export MIR_MESA_KMS_DISABLE_MODESET_PROBE=1
fi

# Set some envs
export QT_QPA_PLATFORM=wayland
export QT_IM_MODULE=maliit
export MALIIT_FORCE_DBUS_CONNECTION=1
export UITK_ICON_THEME=suru
dbus-update-activation-environment --systemd UITK_ICON_THEME=suru
dbus-update-activation-environment --systemd MALIIT_FORCE_DBUS_CONNECTION=1
dbus-update-activation-environment --systemd QT_IM_MODULE=maliit
dbus-update-activation-environment --systemd GTK_IM_MODULE=maliit
dbus-update-activation-environment --systemd QT_QPA_PLATFORM=wayland
dbus-update-activation-environment --systemd SDL_VIDEODRIVER=wayland
dbus-update-activation-environment --systemd QT_WAYLAND_DISABLE_WINDOWDECORATION=1
dbus-update-activation-environment --systemd QT_ACCESSIBILITY=1
dbus-update-activation-environment --systemd QT_AUTO_SCREEN_SCALE_FACTOR=0
dbus-update-activation-environment --systemd GTK_CSD=0

export LOMIRI_AS_SYSTEMD_UNIT=1

ENABLE_UBUNTU_ACCOUNTSSERVICE="@ENABLE_UBUNTU_ACCOUNTSSERVICE@"
USER_LANG=$(gdbus call --system --dest org.freedesktop.Accounts --object-path /org/freedesktop/Accounts/User"$(id -u)" --method org.freedesktop.DBus.Properties.Get org.freedesktop.Accounts.User Language | cut -d\' -f2)

if [ -n "$USER_LANG" ]; then
    export LANGUAGE="$USER_LANG"
    dbus-update-activation-environment --systemd LANGUAGE="$USER_LANG"
fi

if [ "$ENABLE_UBUNTU_ACCOUNTSSERVICE" = "OFF" ]; then
    USER_LOCALE=$(gdbus call --system --dest org.freedesktop.Accounts --object-path /org/freedesktop/Accounts/User"$(id -u)" --method org.freedesktop.DBus.Properties.Get com.lomiri.shell.AccountsService FormatsLocale | cut -d\' -f2)

    if [ -n "$USER_LOCALE" ]; then
        export LANG="$USER_LOCALE"
        dbus-update-activation-environment --systemd LANG="$USER_LOCALE"
    fi
fi

# We need to be sure we dont use mir-on-mir
unset MIR_SERVER_HOST_SOCKET

# start with clean displays
unset DISPLAY

# Claim the user wldisplay
export WAYLAND_DISPLAY=wayland-0
dbus-update-activation-environment --systemd WAYLAND_DISPLAY=${WAYLAND_DISPLAY}

if command -v Xwayland 2>/dev/null; then
  export MIR_SERVER_ENABLE_X11=1
  export MIR_SERVER_XWAYLAND_PATH=/usr/libexec/Xwayland.lomiri
fi

exec ${LOMIRI_BINARY:-lomiri} --mode=$MODE "$@"

